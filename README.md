# Control your Data idea


**IDEAL:**

Detect and store information gathered about you in conventional ways to a repository, to then be able to be plugged into and provided to applications or websites (by choice, not by default), or to be stored and accumulated to be sold in bulk or leased otherwise, unique and auditable data.

Sensors such as pedometers, gyroscopes, GPS and WIFI adapter, modern car utilities(if not the whole damn car), microphones, credit card interactions (maybe) all provide information on where you go, how long a walk is, if you interacted at a shop, probable length of leg and therefore pants size?, average walking speed, average daily heart rate, average amount spent at x or y category shop (Shops to bid for that customer?), car health, foot health, power consumption, power efficiency and i will keep ading to this because I imagine a lot of insights can be learned, or at elast learnt to be learned.

The plan would be to allow people to accumulate data and profit from it if they wish, store that data, and create an economy that powerfully enables the consumer to recieve advertising for things they want,and it will make companies advertsiing budgets more direct.
Statistical algorithms could be developed or employed to even be the source of those insights, creating more value and even monopoly.
After that, from this an AI could be developed from which would be the main interaction with consumers and business. This would take many forms, all stemming from one concept, of placeholder entities to be used in industry.
I envisiion a placeholder entity as a construct for a specific indsutry; that is a default interface to interact with customers in a specific domain, while sharign data to a central repository to aid in future recommmendations etc.
For example, this could take the form of a robot or just an avatar on a screen, greeting customers with things they may (based on an open and instant data communication as they approach) enjoy or may be of interest. This is rife through science fiction movies and shows when showing a utopian future.
-----------------

**STEPS:**

 1. Create a barrier for browser cookies or local storage or otherwise (probably find that out) to be read and set. Maybe this means making a browser plugin or even a proprietary browser. Dunno yet.
 2. Create a barrier for sensors on a smartphone to communicate with and give permission to other applications.
 3. Collect that data and negotiate with aforementioned applications and companies for access.
 4. Start to collect or buy data off customers to begin data mining bore hole in the world.
 5. Create multiple barriers for multple devices in the easiest way possible to install/use. (Float around Android market, Apple probably wouldnt like it.)
 6. Support an overwhelming array of platforms, and/or make a basic version open source for use with other applications for people to build and develop themselves for their own use. (Commercial use requires licences/api key etc?)
 7. Possibly partner with insurance companies to have their customers live stream their data for discounts when proven to be in no harm etc.


One of the most important things is that it must be opt-in and if this is installed, can act as a way to stop data collection on you or make you aware of applications that are capturing data that you werent aware of.
This may be a good way to make it popular within the technical community and have them develop it further. 

The root of this is the enabling of the accumulation and perssonal responsibility of data, enabling to profit off it, then learning from it to sell to business customers, and then expanding with proprietary products for industry specific applications as an interface to dominate retail advertising, and therefore rule the world. 
This is probably how SkyNet was developed. It is noted that a side project must be developed concurrently to create a Terminator like robot to destroy it if it ever goes rogue.


**TO DO:** 

- List all sensors that can accumulate data in common items like smart phones, watches cars etc.


